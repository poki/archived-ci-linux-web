<H1>keepalive(1M)</H1>
<HR>
<B>keepalive -- monitor and respawn processes and daemons</B>
<P>
<H2>Synopsis</H2>
<B>keepalive</B> [<B>-i</B>] [<B>-t</B> <B><I>interval</I></B>] [<B>-P</B> <B><I>node</I></B>] [<B>-S</B> <B><I>node</I></B>]
<H2>Description</H2>
The <B>keepalive</B> daemon monitors processes and daemons that are
registered with 
it using the <B>spawndaemon</B>(1M) 
utility. When a registered process or daemon fails, 
<B>keepalive</B> logs the event using 
<B>syslog</B>(3G), and normally 
executes a restart script to restart the process/daemon. 
For a complete description of 
the process monitoring and restart features available with <B>keepalive</B>, 
refer to  
<B>spawndaemon</B>(1M).
<P>
<B>keepalive</B> is more versatile than 
<B>init</B>(1M) for respawning processes. 
Advantages of using <B>keepalive</B> include:
<UL>
<LI>Provides a command-line interface (<B>spawndaemon</B>) with many 
process/daemon restart options.</LI>

<LI>Monitors real-time processes; it is completely signal driven.</LI>

<LI>Monitor daemons.</LI>

<LI>Restarts a process/daemon on any node in the cluster with a 
user-specified node selection policy.</LI>
</UL>
<B>keepalive</B>, which is started by 
<B>init</B>(1M), uses a memory-mapped 
data file, <B>/etc/keepalive.d/keepalive.data</B> (referred to as the 
monitored process table), to track the state of 
processes/daemons it is monitoring. Restart <B>keepalive</B> in any script 
invoked by 
<B>init</B>(1M) or 
<B>shutdown</B>(1M). 
To kill <B>keepalive</B> permanently, you must 
execute <B>spawndaemon</B> with the <B>-Q</B> option and edit
<B>/etc/inittab</B> to remove the <B>keepalive</B> entries.
<P>
<B>keepalive</B> uses <B>syslog</B> to log messages. The <I><B>ident</B></I> 
string 
is <B>keepalive</B>, and the <I><B>facility</B></I> string is <B>LOG_DAEMON</B>. 
This information is provided for customizing 
<B>syslogd</B>(1M) 
configuration.
<P>
The <B>keepalive</B> daemon uses standard shell scripts to restart 
processes/daemons 
that have terminated. These scripts can have any file name, but must be 
stored in the <B>/etc/keepalive.d</B> directory. Their group and user IDs 
must be <B>root</B> and their permissions set to 0755 to disallow write 
access by others.
<P>
If the executable for the process/daemon resides in a remote system, 
you must have root access to start or restart the process. The remote file 
system must be shared or exported with root permissions enabled. Root 
permissions must also be enabled on the mount point, the automount table, 
or the <B>/etc/vfstab</B> file.
<P>
Any process/daemon started by <B>keepalive</B> has
its <B>stdout</B> and <B>stderr</B> redirected to a logging file called
<B>/var/log/keepalive/</B><B><I>daemon_basename</I></B><B>.</B><B><I>process_id</I></B>.
You should call 
<B>fflush</B>(3S) on <B>stdout</B> 
and <B>stderr</B> to
force data to disk. <B>stdin</B> for the process/daemon is redirected to
<B>/</B> (root), causing any attempts to read from <B>stdin</B> to
fail. 
<P>
Files under <B>/var/log/keepalive</B> are not allowed to accumulate.
Unregistering a process/daemon causes its logging file to be deleted. If a
process/daemon is restarted, the logging file of the previous instance of the
process/daemon is deleted. If <B>keepalive</B> is started with the <B>-i</B>
option or is shutdown, all logging files not in use are deleted.
<P>
If you remove the memory-mapped data file (monitored process table) belonging 
to the <B>keepalive</B> 
daemon, you should also shut down the <B>keepalive</B> daemon with the
<B>spawndaemon -Q</B> command. To shut down <B>keepalive</B>, but leave
the monitored process table intact, send the <B>keepalive</B> daemon a
SIGTERM signal so it performs a controlled exit.

<H3>Options</H3>
The <B>keepalive</B> command uses the following options and arguments:
<DL COMPACT>
<DT><B>-i</B><DD>Removes the memory-mapped data file, 
<B>/etc/keepalive.d/keepalive.data</B>, 
before starting the <B>keepalive</B> daemon. Using the <B>-i</B> option 
causes the <B>keepalive</B> daemon to start up with a clean monitored process 
table, such that it is not monitoring any processes.
<P><DT><B>-t</B> <B><I>interval</I></B><DD>
Defines the time in seconds that <B>keepalive</B> uses as a polling interval 
in rare cases where <B>keepalive</B> must use polling (such as when a call to 
<B>fork</B>(2) fails due to 
a node resource problem). The default value is <B>5</B>. Note that if a
<TT>fork</TT> fails on a node, <B>keepalive</B> tries other nodes in the node
set for the process/daemon being (re)started. See
<B>spawndaemon</B>(1M) for details.
<P><DT><B>-P</B> <B><I>node</I></B><DD>
Specifies the primary node on which the <B>keepalive</B> process is 
executed. The <B>keepalive</B> process is pinned on the specified 
node and cannot be migrated by using the 
<B>load_leveld</B>(1) utility.
<P><DT><B>-S</B> <B><I>node</I></B><DD>
Specifies the secondary node on which the <B>keepalive</B> process is
executed when the primary node is unavailable. The <B>keepalive</B>
process is pinned on the specified node and cannot be migrated by
using the <B>load_leveld</B>(1) 
utility.
</DL>
<P>
If one of the specified nodes is down or invalid, <B>keepalive</B> logs
a warning and continues execution on another available node.
<P>
A <B>keepalive</B> process is always pinned on the node on which it is 
running, regardless of whether or not the user specifies a node. If a
primary or secondary node is not specified on the command line, the
system chooses any available node.
<H2>Files</H2>
<DL COMPACT>
<DT><B>/dev/keepalivecfg</B><DD> 
Named pipe for receiving commands
<P><DT><B>/etc/keepalive.d</B><DD> 
Directory for process/daemon restart scripts
<P><DT><B>/etc/keepalive.d/keepalive.data</B><DD>
The <B>keepalive</B> memory-mapped data file (monitored process table) 
for tracking the state of processes/daemons being monitored.
<P><DT><B>/var/log/keepalive</B><DD>
Directory containing files into which any process/daemon started by
<B>keepalive</B> has its <B>stdout</B> and <B>stderr</B> redirected. 
</DL>
<H2>References</H2>
<B>fflush</B>(3S),
<B>init</B>(1M),
<B>load_leveld</B>(1), 
<B>spawndaemon</B>(1M),
<B>syslogd</B>(1M), 
<B>syslog</B>(3G),  
<B>vfstab</B>(4)
<!-- NAVBEGIN -->
<HR>
<I>
<SMALL>
15 August 2001
<BR>
Copyright 2001 Compaq Computer Corporation
<BR>
Cluster-Tools Version 0.5.8
</SMALL>
</I>
